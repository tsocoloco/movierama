import React, {Component} from 'react'
import {Navbar, Nav, Alert} from 'react-bootstrap'
import {showSignupAction, showLoginAction , logoutUserAction} from '../actions'
import {connect} from "react-redux";

class AppHeader extends Component {
  render() {

    // const alert = (<Alert variant={'success'}>Welcome {this.state.email}!</Alert>)

    // const logged = isUserLoggedIn ? (Logged in as <Nav.Link >koko</Nav.Link> : '';
    const user = this.props.username;

    let navlInks = user !=='' ? (<Navbar className={'row'}> Welcome <Nav.Link href="">{user}</Nav.Link> <Nav.Link href="/" onClick={()=>this.props.logoutUserAction()}>Logout</Nav.Link></Navbar>) : (
        <div className={'row'}>
          <Nav.Link
              onClick={() => this.props.showLoginAction(true)}>Login</Nav.Link><Nav.Link onClick={() => this.props.showSignupAction(true)}>Sign Up</Nav.Link>
        </div>);
    return (
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand>MovieRama</Navbar.Brand>
          <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
            {navlInks}
          </Navbar.Collapse>
        </Navbar>
    )
  }
}


const mapStateToProps = state => {
  return {
    isUserLoggedIn: state.appReducer.app.isUserLoggedIn,
  }
};

export default connect(
    mapStateToProps,
    {showLoginAction, showSignupAction, logoutUserAction}
)(AppHeader);
