import React, {Component} from 'react'
import {Navbar, Nav, Alert} from 'react-bootstrap'
import {showSignupAction, showLoginAction , logoutUserAction} from '../actions'
import {connect} from "react-redux";

class SortItem extends Component {
  render() {
    return (
        <div>Sort by: <a>Likes</a>| <a>Hates</a> |<a>Date</a>  </div>
    )
  }
}


const mapStateToProps = state => {
  return {
    isUserLoggedIn: state.appReducer.app.isUserLoggedIn,
  }
};

export default connect(
    mapStateToProps,
    {showLoginAction, showSignupAction, logoutUserAction}
)(SortItem);
