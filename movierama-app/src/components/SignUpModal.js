import React, {Component} from 'react'
import {Modal, Button, Form} from 'react-bootstrap'
import {connect} from "react-redux";
import {showSignupAction, signupUserAction} from '../actions'

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const {name, value} = e.target;
    this.setState({[name]: value});
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({submitted: true});
    const {email, password} = this.state;
    const {signupUserAction} = this.props;
    if (email && password) {
      signupUserAction({email, password, username:email});
    }
  }
componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.userData !== this.props.userData) {
      this.props.showSignupAction(false);

    }
}

  render() {

    const buttonStyle = {
      margin: '10px'
    };

    const {email, password} = this.state;
    return (
        <Modal
            show={this.props.signupModalShow}
            onHide={() => this.props.showSignupAction(false)}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              SignUp
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" name="email" placeholder="Enter email" value={email}
                              onChange={this.handleChange}/>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" name="password" placeholder="Password" value={password}
                              onChange={this.handleChange}/>
              </Form.Group>
              <Button variant="primary" type="submit" style={buttonStyle}>Sign
                Up</Button>
              <Button style={buttonStyle} variant="primary"
                      onClick={() => this.props.showSignupAction(false)}>Close</Button>
            </Form>
          </Modal.Body>
        </Modal>
    )
  }
}

const mapStateToProps = state => {
  return {
    signupModalShow: state.appReducer.signupModalShow,
    signupUserAction: state.appReducer.signupUserAction,
    userData: state.userReducer.userData
  }
};

export default connect(
    mapStateToProps,
    {showSignupAction, signupUserAction}
)(SignUpForm);
