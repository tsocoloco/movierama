import React, {Component} from 'react'
import {Modal, Button, Form} from 'react-bootstrap'
import {connect} from "react-redux";
import {showLoginAction, loginUserAction} from '../actions'

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const {name, value} = e.target;
    this.setState({[name]: value});
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({submitted: true});
    const {email, password} = this.state;
    const {loginUserAction} = this.props;
    if (email && password) {
      loginUserAction({email, password});
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.userData !== this.props.userData) {
      this.props.showLoginAction(false);
    }
  }

  render() {

    const buttonStyle = {
      margin: '10px'
    };
    const {email, password} = this.state;

    return (
        <Modal
            show={this.props.showLogin}
            onHide={() => this.props.showLoginAction(false)}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Login
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control name="email" type="email" placeholder="Enter email" value={email}
                              onChange={this.handleChange}/>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control name="password" type="password" placeholder="Password" value={password}
                              onChange={this.handleChange}/>
              </Form.Group>

              <Button variant="primary" type="submit" style={buttonStyle}>
                Login
              </Button>

              <Button style={buttonStyle} variant="primary"
                      onClick={() => this.props.showLoginAction(false)}>Close</Button>

            </Form>
          </Modal.Body>
        </Modal>
    )
  }
}

const mapStateToProps = state => {
  return {
    showLogin: state.appReducer.loginModalShow,
    loginUserAction: state.appReducer.loginUserAction,
    userData: state.userReducer.userData
  }
};

export default connect(
    mapStateToProps,
    {showLoginAction, loginUserAction}
)(LoginForm);
