import React, {Component} from 'react'
import MovieItem from "./MovieItem";
import _ from 'lodash'
import Spinner from "react-bootstrap/Spinner";

export default class MovieList extends Component {
  render() {
    const style = {
      // width: 600,
      height: 'auto',
      margin: 'auto',
      textAlign: 'center',
    };
    const {movie_list} = this.props;

    let comp = (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>);

    const items = _.map(movie_list, movie => {
      return (<MovieItem key={movie.id} movie={movie}/>)
    });
    if (items.length > 0) {
      comp = (<div style={style} key={1}>{items}</div>);
    }
    return (<div>{comp}</div>)
  }
}
