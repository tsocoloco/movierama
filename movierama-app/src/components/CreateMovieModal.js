import React, {Component} from 'react'
import {Modal, Button, Form} from 'react-bootstrap'
import {connect} from "react-redux";
import {showAddMovieModalAction, addMovieAction} from '../actions'

class CreateMovieModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const {name, value} = e.target;
    this.setState({[name]: value});
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({submitted: true});
    const {title, description} = this.state;
    const {addMovieAction} = this.props;
    if (title && description) {
      addMovieAction({title, description, user_id: this.props.userData.user.id});
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.added_movie !== this.props.added_movie) {
      this.props.showAddMovieModalAction(false);
    }
  }

  render() {

    const buttonStyle = {
      margin: '10px'
    };
    const {title, description} = this.state;

    return (
        <Modal
            show={this.props.addMovieModalShow}
            onHide={() => this.props.showAddMovieModalAction(false)}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Add New Movie
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control name="title" type="text" placeholder="Enter title" value={title}
                              onChange={this.handleChange}/>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Description</Form.Label>
                <Form.Control name="description" type="text" placeholder="Description" value={description}
                              onChange={this.handleChange}/>
              </Form.Group>

              <Button variant="primary" type="submit" style={buttonStyle}>
                Add Movie
              </Button>

              <Button style={buttonStyle} variant="primary"
                      onClick={() => this.props.showAddMovieModalAction(false)}>Close</Button>

            </Form>
          </Modal.Body>
        </Modal>
    )
  }
}

const mapStateToProps = state => {
  return {
    addMovieModalShow: state.appReducer.addMovieModalShow,
    userData: state.userReducer.userData,
    added_movie: state.movieReducer.added_movie,
  }
};

export default connect(
    mapStateToProps,
    {showAddMovieModalAction, addMovieAction}
)(CreateMovieModal);
