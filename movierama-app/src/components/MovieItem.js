import React, {Component} from 'react'

import {connect} from "react-redux";
import {fetchUserMovies, voteMovieAction, fetch_movies, unVoteMovieAction} from "../actions";
import {Row, Col} from "react-bootstrap";
import _ from 'lodash';

class MovieItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId: '',
      isOwnVote: []
    }
  }

  isUserLoggedin = () => {
    return (this.props.userData && typeof this.props.userData.user !== "undefined");
  };


  isVoterAllowed = (userId) => {
    return userId !== this.state.userId;
  };

  componentWillReceiveProps(nextProps, nextContext) {
    // console.log('nextProps', nextProps)
    if (nextProps.user_votes !== this.props.user_votes) {
      this.props.fetch_movies();
    }
    if (nextProps.userData.user !== this.props.userData.user && typeof nextProps.userData.user !== "undefined") {
      this.setState({
        userId: nextProps.userData.user.id
      })
    }
    if(nextProps.totalUserVotes !== this.props.totalUserVotes){
      this.forceUpdate();
    }
    if (nextProps.totalUserVotes !== this.props.totalUserVotes && this.props.totalUserVotes.length > 0 && typeof this.props.totalUserVotes !== "string") {
      this.setState({
        isOwnVote: this.props.totalUserVotes.filter((rec) => {
          return rec.user_id === this.state.userId && rec.movie_id === this.props.movie.id;
        })
      })
    }
  }

  unvoteFunc = (userId,id) => {
    this.props.unVoteMovieAction({
      user_id: userId,
      movie_id: id
    });
    this.forceUpdate();
  };

  render() {
    const style = {
      width: 400,
      height: 'auto',
      margin: 'auto',
      marginTop: 20,
      padding: 10,
      border: '1px solid',
      header: {
        textAlign: 'left',
        fontWeight: '400',
        fontSize: 40
      },
      postedBy: {},
      body: {
        margin: 20,
        // fontSize: 30
      },
      impressions: {
        // marginRight: '270px'
      }
    };
    const {id, title, description, username, created_at, movie_likes, movie_hates, user_id} = this.props.movie;
    const {userId} = this.state;
    const days = new Date(new Date().getTime() - new Date(created_at).getTime()).getDate();
    const likeComp = (<a href="#" onClick={(e) => {
      e.preventDefault();
      return this.isVoterAllowed(user_id) ? this.props.voteMovieAction({
        user_id: userId,
        movie_id: id,
        vote_type: 'like'
      }) : null
    }}>likes</a>);
    const hateComp = (<a href="#" onClick={(e) => {
      e.preventDefault();
      return this.isVoterAllowed(user_id) ? this.props.voteMovieAction({
        user_id: userId,
        movie_id: id,
        vote_type: 'hate'
      }) : null
    }}>hates</a>);

    const notLoggedInImpressions = (<div style={style.impressions}> {movie_likes} likes | {movie_hates} hates </div>);
    const loggedIdImpressions = (
        <div style={style.impressions}> {movie_likes} {likeComp} | {movie_hates} {hateComp}</div>);

    const isOwn = this.state.isOwnVote;

    const likeHateComp = isOwn.length > 0 && typeof this.props.totalUserVotes !=='string'? (
        <div>You already {isOwn[0].vote_type} this | <a href="#" onClick={(e) => {
          e.preventDefault();
          return this.unvoteFunc(userId,id);
        }}>Undo</a></div>) : null;

    // console.log('this.state', this.state)
    return (<div style={style} key={id}>
      <div style={style.header}>{title}</div>
      <div style={style.postedBy}>
        Posted by <a href="#"
                     onClick={() => this.props.fetchUserMovies(user_id)}>{username || 'user'}</a> {days === 0 ? 'today' : `${days} days ago`}
      </div>
      <div style={style.body}>{description}</div>
      <Row>
        <Col xs={4}>
          {this.isUserLoggedin() && this.isVoterAllowed(user_id) ? loggedIdImpressions : notLoggedInImpressions}
        </Col>
        <Col xs={8}>
          {likeHateComp}
        </Col>
      </Row>
    </div>)
  }
}

const mapStateToProps = state => {
  return {
    userData: state.userReducer.userData,
    totalUserVotes: state.userReducer.totalUserVotes,
    user_votes: state.movieReducer.user_votes
  }
};

export default connect(
    mapStateToProps,
    {fetchUserMovies, voteMovieAction, fetch_movies, unVoteMovieAction}
)(MovieItem);
