import {
  addNewMovie,
  authWithToken,
  getAllMovies,
  getUserMovies, getUserVotes,
  loginUser,
  signupUser,
  voteMovie,
  unVoteMovie
} from '../managers/apiManager'

export const GET_ALL_MOVIES = 'GET_ALL_MOVIES';
export const GET_USER_MOVIES = 'GET_USER_MOVIES';

export const SHOW_LOGIN = 'SHOW_LOGIN';
export const SHOW_SIGNUP = 'SHOW_SIGNUP';


export const LOGIN_USER = 'LOGIN_USER';
export const SIGNUP_USER = 'SIGNUP_USER';
export const LOGOUT_USER = 'LOGOUT_USER';
export const AUTH_USER_TOKEN = 'AUTH_USER_TOKEN';
export const GET_USER_VOTES = 'GET_USER_VOTES';

export const ADD_MOVIE = 'ADD_MOVIE';
export const SHOW_ADD_MOVIE = 'SHOW_ADD_MOVIE';
export const VOTE_MOVIE = 'VOTE_MOVIE';
export const UNVOTE_MOVIE = 'UNVOTE_MOVIE';


export function fetch_movies() {
  return (dispatch) => {
    getAllMovies().then(data => {
      dispatch({
        type: GET_ALL_MOVIES,
        data: data || 'No movies available'
      })
    });

  }
}

export function fetchUserMovies(userId) {
  return (dispatch) => {
    getUserMovies(userId).then(data => {
      dispatch({
        type: GET_USER_MOVIES,
        data: data || 'No movies available'
      })
    });

  }
}

export function showSignupAction(status) {
  return {
    type: SHOW_SIGNUP,
    show: status
  }
}

export function showLoginAction(status) {
  return {
    type: SHOW_LOGIN,
    show: status
  }
}


export function showAddMovieModalAction(status) {
  return {
    type: SHOW_ADD_MOVIE,
    show: status
  }
}

export function loginUserAction(userData) {
  return (dispatch) => {
    loginUser(userData).then(data => {
      dispatch({
        type: LOGIN_USER,
        data: data
      });
      if (data) {
        localStorage.setItem('appToken', data.data.token);
      }
    });
  }
}


export function signupUserAction(userData) {
  return (dispatch) => {
    signupUser(userData).then(data => {
      dispatch({
        type: SIGNUP_USER,
        data: data
      });
      showSignupAction(false);
      if (data.user) {
        localStorage.setItem('appToken', data.token);
      }
    });
  }
}

export function authTokenAction() {
  return (dispatch) => {
    authWithToken().then(data => {
      dispatch({
        type: AUTH_USER_TOKEN,
        data: data
      });
    });
  }
}


export function logoutUserAction() {
  localStorage.removeItem('appToken');
  return {
    type: LOGOUT_USER,
    data: {}
  }
}

export function addMovieAction(movieData) {
  return (dispatch) => {
    addNewMovie(movieData).then(data => {
      dispatch({
        type: ADD_MOVIE,
        data: data
      })
    });

  }
}

export function voteMovieAction(movieData) {
  return (dispatch) => {
    voteMovie(movieData).then(data => {
      dispatch({
        type: VOTE_MOVIE,
        data: data
      })
    });
  }
}


export function unVoteMovieAction(movieData) {
  return (dispatch) => {
    unVoteMovie(movieData).then(data => {
      dispatch({
        type: UNVOTE_MOVIE,
        data: data
      })
    });
  }
}

export function getUserVotesAction(userId) {
  return (dispatch) => {
    getUserVotes(userId).then(data => {
      dispatch({
        type: GET_USER_VOTES,
        data: data
      })
    });
  }
}

