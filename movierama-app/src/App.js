import React, {Component} from 'react';
import {connect} from 'react-redux'
import logo from './logo.svg';
import './App.css';
import {fetch_movies, showAddMovieModalAction, fetchUserMovies, authTokenAction, getUserVotesAction} from './actions'
import MovieList from "./components/MovieList";
import AppHeader from "./components/Header";
import 'bootstrap/dist/css/bootstrap.min.css'
import LoginForm from "./components/LoginModal";
import SignUpForm from "./components/SignUpModal";
import SortItem from "./components/SortItem";
import {Toast, Alert} from "react-bootstrap/";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import CreateMovieModal from "./components/CreateMovieModal";
import _ from 'lodash';
import Spinner from "react-bootstrap/Spinner";
import Pusher from 'pusher-js'
import Notifications, {notify} from 'react-notify-toast';

var pusher = new Pusher('95b33f712375b4aabd0a', {
  cluster: 'eu',
  forceTLS: true
});

var channel = pusher.subscribe('movie-channel');


class App extends Component {

  constructor(props) {
    super(props);
    const token = localStorage.getItem('appToken') || '';
    if (token !== '') {
      this.props.authTokenAction();
    }

    this.state = {
      username: '',
      userId: '',
      showAlert: false,
      alertMsg: '',
      movie_list: [],
      token: ''
    };
    this.closeAlert = this.closeAlert.bind(this);
  }

  refreshMovieList = () => {
    if (this.state.username === '') {
      this.props.fetch_movies();
    } else {
      this.props.fetchUserMovies(this.props.userData.user.id);
    }
    this.setState({movie_list: this.props.movie_list})
  };

  isUserLoggedin = () => {
    return (this.props.userData && typeof this.props.userData.user !== undefined);
  };

  componentDidMount() {
    this.refreshMovieList();
    this.setState({
      movie_list: this.props.movie_list
    });

  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.user_votes !== this.props.userData && typeof this.props.userData.user !== "undefined") {
      this.props.getUserVotesAction({user_id: nextProps.userData.user.id});
    }
  }

  componentDidUpdate(nextProps, nextContext) {
    if (nextProps.userData !== this.props.userData && typeof nextProps.userData.user !== undefined && nextProps.userData.user !== this.props.userData.user) {
      this.setState({
        showAlert: false,
        username: this.props.userData.user.username,
        userId: this.props.userData.user.id
      })
    } else if (nextProps.added_movie !== this.props.added_movie) {
      this.refreshMovieList();
    } else if (nextProps.movie_list !== this.props.movie_list) {
      this.setState({
        movie_list: this.props.movie_list
      })
    } else if (nextProps.userData !== this.props.userData && typeof nextProps.userData.user === undefined && typeof nextProps.userData.err !== undefined) {
      this.setState({
        showAlert: true,
        alertMsg: nextProps.userData.err || 'Error'
      })
    }
  }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    console.log(error, info);
  }

  closeAlert() {
    setTimeout(() => {
      this.setState({
        showAlert: false,
        alertMsg: ''
      })
    }, 3000)
  }

  sortMovies = (movie_list, sortBy) => {
    this.setState({
      movie_list: _.sortBy(movie_list, [sortBy])
    })
  };


  render() {
    let voteAlert;
    channel.bind('vote', function(data) {
      // alert(JSON.stringify(data));
      notify.show(`${data.message}!`);
    });
    const {movie_list} = this.props;
    const {showAlert, username} = this.state;
    const handleShow = () => this.setState({showAlert: true});
    const handleClose = () => this.setState({showAlert: false, alertMsg: ''});
    const alert = this.state.showAlert ? (
        <Alert variant="danger" onClose={handleClose} show={showAlert} style={{
          position: 'absolute',
          top: 60,
          right: 0,
          // backgroundColor: 'red',
          zIndex: 100000,
          width: 150
        }} dismissible>
          {this.state.alertMsg}
        </Alert>
    ) : null;

    const sortComponent = (
        <div className={'sort-component'}>
          Sort by:
          <a href='#' onClick={() => this.sortMovies(movie_list, 'movie_likes')}>Likes</a> &nbsp;|&nbsp;
          <a href='#' onClick={() => this.sortMovies(movie_list, 'movie_hates')}>Hates</a> &nbsp;|&nbsp;
          <a href='#' onClick={() => this.sortMovies(movie_list, 'created_at')}>Date</a>
        </div>);
    return (
        <div className="App">
          <AppHeader username={username}/>
          <Container>
            <Row>
              {sortComponent}
            </Row>
            <Row>
              <Col xs={username === '' ? 12 : 8}>
                <MovieList movie_list={this.state.movie_list}/>
              </Col>
              <Col xs={username === '' ? 0 : 4}>
                <Button className={'create-movie-button'} variant="success"
                        onClick={() => this.props.showAddMovieModalAction(true)}
                        hidden={username === ''}>Add new movie</Button>
              </Col>
            </Row>

          </Container>
          <LoginForm/>
          <SignUpForm/>
          <CreateMovieModal/>
          {alert}
        <Notifications/>
        </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    movie_list: state.movieReducer.movie_list,
    added_movie: state.movieReducer.added_movie,
    showLogin: state.appReducer.loginModalShow,
    userData: state.userReducer.userData,
  }
};


export default connect(
    mapStateToProps,
    {fetch_movies, showAddMovieModalAction, fetchUserMovies, authTokenAction, getUserVotesAction}
)(App);
