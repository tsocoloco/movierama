import * as actions from '../actions'

let initialState = {
    loginModalShow: false,
    signupModalShow: false,
    addMovieModalShow: false,
    app: {
        isFetching: false,
        isUserLoggedIn: false
    }
};

const appReducer = (state = initialState, action = {}) => {

    switch (action.type) {
        case actions.SHOW_SIGNUP:
            return {
                ...state,
                signupModalShow: action.show
            }
        case actions.SHOW_LOGIN:
            return {
                ...state,
                loginModalShow: action.show
            }
        case actions.SHOW_ADD_MOVIE:
            return {
                ...state,
                addMovieModalShow: action.show
            }

        default:
            return {...state}
    }

};
export default appReducer;
