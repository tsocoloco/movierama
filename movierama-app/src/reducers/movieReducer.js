import * as actions from '../actions'

let initialState = {
  movie_list: [],
  added_movie: {},
  user_votes: {}
};

const movieReducer = (state = initialState, action = {}) => {

  switch (action.type) {
    case actions.GET_ALL_MOVIES:
      return {
        ...state,
        movie_list: action.data
      };
    case actions.GET_USER_MOVIES:
      return {
        ...state,
        movie_list: action.data
      };
    case actions.ADD_MOVIE:
      return {
        ...state,
        added_movie: action.data
      };
    case actions.VOTE_MOVIE:
      return {
        ...state,
        user_votes: Object.assign(action.data, initialState.user_votes)
      };
    case actions.UNVOTE_MOVIE:
      return {
        ...state,
        user_votes: Object.assign(action.data, initialState.user_votes)
      };
    default:
      return {...state}
  }

};
export default movieReducer;
