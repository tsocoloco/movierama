import {combineReducers} from 'redux'
import movieReducer from './movieReducer';
import appReducer from './appReducer';
import userReducer from './userReducer';

const rootReducer = combineReducers({
  movieReducer,
  appReducer,
  userReducer
});
export default rootReducer;
