import * as actions from '../actions'

let initialState = {
  userData: {},
  totalUserVotes: {}
};

const userReducer = (state = initialState, action = {}) => {

  switch (action.type) {
    case actions.LOGIN_USER:
      // console,
      return {
        ...state,
        userData: action.data.data,
      };
    case actions.SIGNUP_USER:
      return {
        ...state,
        userData: action.data,
      };
    case actions.LOGOUT_USER:
      return {
        ...state,
        userData: action.data,
      };
    case actions.AUTH_USER_TOKEN:
      return {
        ...state,
        userData: action.data,
      };
    case actions.GET_USER_VOTES:
      return {
        ...state,
        totalUserVotes: action.data,
      };
    default:
      return {...state}
  }

};
export default userReducer;
