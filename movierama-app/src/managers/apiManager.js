const host = process.env.NODE_ENV === 'production' ? 'https://movierama-api.herokuapp.com' : 'http://localhost:3001';

export const getAllMovies = () => {
  // console.log(' calling get data')
  return fetch(host + '/v1/movies')
      .then(res => res.json());
};

export const getUserMovies = async (userId) => {
  // console.log(' calling get data')
  const token = await localStorage.getItem('appToken');
  return fetch(host + `/v1/movies/user/${userId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
      .then(res => res.json());
};

export const addNewMovie = async (data) => {
  const token = await localStorage.getItem('appToken');
  return fetch(host + '/v1/movies', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + token
    },
    body: JSON.stringify(data)
  })
      .then(res => res.json());
};


export const voteMovie = async (data) => {
  const token = await localStorage.getItem('appToken');
  return fetch(host + '/v1/movies/vote', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + token
    },
    body: JSON.stringify(data)
  })
      .then(res => res.json());
};


export const unVoteMovie = async (data) => {
  const token = await localStorage.getItem('appToken');
  return fetch(host + '/v1/movies/unvote', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + token
    },
    body: JSON.stringify(data)
  })
      .then(res => res.json());
};

export const loginUser = (user) => {
  // console.log('login',user)
  return fetch(host + '/v1/users/login', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(user)
  })
      .then(res => res.json());
};
export const signupUser = (user) => {
  // console.log('user',user)
  return fetch(host + '/v1/users/', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(user)
  })
      .then(res => res.json());
};


export const authWithToken = async () => {
  const token = await localStorage.getItem('appToken');
  return fetch(host + '/v1/users/checkToken', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + token
    },
  })
      .then(res => res.json());
};


export const getUserVotes = async (data) => {
  const token = await localStorage.getItem('appToken');
  return fetch(host + `/v1/users/${data.user_id}/votes`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'authorization': 'Bearer ' + token
    },
  })
      .then(res => res.json());
};
