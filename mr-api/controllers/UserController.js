const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const saltRounds = 12;
const secret = process.env.APP_SECRET;

const hashPassword = text => bcrypt.hash(text, saltRounds)
    .then(hash => hash);

const generateToken = email => new Promise((res, rej) => jwt.sign({email}, secret, {expiresIn: '90 days'}, (err, token) => {
  if (err) rej(err);
  res(token);
}));

const createUser = async (req, res, next) => {
  const {password, username, email} = req.body;
  if (!password || !email || !username) {
    res.status(400)
        .send({msg: 'Missing data'});
    return null;
  }
  logger.info(`[${email}] - Attempt to POST in user_accounts`);
  const [hashedPassword, token] = await Promise.all([hashPassword(password), generateToken(email)]);
  DB.users.insert({
    username,
    email,
    password: hashedPassword,
  })
      .then((user) => {
        logger.info(`User account for [${email}] created`);
        const data = {
          user,
          token,
        };
        res.status(200).send(data);
      })
      .catch((e) => {
        const errMsg = {
          msg: `[${email}] - Failed to POST user_accounts`,
          err: JSON.stringify(e),
        };
        logger.error(errMsg);
        res.status(400)
            .send(errMsg);
      });
};


const userLogin = async (req, res, next) => {
  const {email, password} = req.body;
  logger.info(`[${email}] - Attempt to GET user`);
  DB.users.find({email})
      .then((account) => {
        if (!account.length > 0) {
          const errMsg = {
            err: `Account for ${email} not found`,
            code: 404,
          };
          res.status(404)
              .json(errMsg);
          return;
        }
        logger.info(`[${email}] - Success GET user`);

        bcrypt.compare(password, account[0].password, async (err, result) => {
          if (result) {
            const token = await generateToken(email);
            const data = {
              user: {
                id: account[0].id,
                email: account[0].email,
                username: account[0].email,
              },
              token
            };
            res.status(200)
                .json({data});
          }
        });
      })
      .catch((e) => {
        const errMsg = {
          msg: `[${email}] - Failed to fetch user_accounts`,
          err: JSON.stringify(e),
        };
        logger.error(errMsg);
        res.status(404)
            .json(errMsg);
      });
};


const checkUser = async (req, res, next) => {
  const {email, username} = req.body;
  logger.info(`[${email}] - Checking user data`);
  const foundEmail = await DB.users.find({email});
  const foundUsername = await DB.users.find({username});

  if (foundEmail.length > 0) {
    const msg = `User already exists with email [${email}]`;
    logger.error(msg);
    res.status(200)
        .json(msg);
  } else if (foundUsername.length > 0) {
    const msg = `User already exists with username [${username}]`;
    logger.error(msg);
    res.status(200)
        .json(msg);
  } else {
    const msg = `User not found`;
    logger.error(msg);
    res.status(404)
        .json(msg);
  }
};


const getUserData = async (req, res, next) => {
  const {email} = req.decoded;
  logger.info(`[${email}] - Checking user data`);
  const foundEmail = await DB.users.find({email});

  if (foundEmail.length > 0) {
    const msg = `User already exists with email [${email}]`;
    logger.trace(msg);
    const data = {
      user: {
        id: foundEmail[0].id,
        email: foundEmail[0].email,
        username: foundEmail[0].email,
      }
    };
    res.status(200)
        .json(data);
  } else {
    const msg = `User not found`;
    logger.error(msg);
    res.status(404)
        .json(msg);
  }
};

const getUserVotes = async (req, res, next) => {
  const user_id = req.params.userId;
  logger.info(`[${user_id}] - Get user votes`);
  const user_votes = await DB.user_votes.find({user_id});

  if (user_votes.length > 0) {
    res.status(200)
        .json(user_votes);
  } else {
    const msg = `User has not voted yet`;
    logger.trace(msg);
    res.status(200)
        .json(msg);
  }
};

module.exports = {createUser, userLogin, checkUser, getUserData, getUserVotes};
