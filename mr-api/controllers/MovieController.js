const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const Pusher = require('pusher');

const pusher = new Pusher({
  appId: '819996',
  key: '95b33f712375b4aabd0a',
  secret: '13931f8526492aacea56',
  cluster: 'eu',
  encrypted: true
});

const createMovie = async (req, res, next) => {
  logger.debug(req.body)
  const {title, description, user_id} = req.body;
  if (!title || !description || !user_id) {
    res.status(400)
        .send({msg: 'Missing data'});
    return null;
  }
  logger.info(`Attempt to POST in movies`);

  DB.movies.insert({
    title, description, user_id,
  })
      .then((movie) => {
        logger.info(`Movie created`);
        res.status(200)
            .send(movie);
      })
      .catch((e) => {
        const errMsg = {
          msg: `Failed to insert movie`,
          err: JSON.stringify(e),
        };
        logger.error(errMsg);
        res.status(400)
            .send(errMsg);
      });
};


const getMovies = async (req, res, next) => {
  logger.info(`Attempt to GET movies`);
  DB.user_movies_view.find({})
      .then((movies) => {
        if (!movies.length > 0) {
          const errMsg = {
            err: `Movies not found`,
            code: 404,
          };
          res.status(404)
              .json(errMsg);
          return;
        } else {
          logger.debug(`Sending ${movies.length} movies`)
          res.status(200)
              .json(movies);
        }
      })
      .catch((e) => {
        const errMsg = {
          msg: `Failed to fetch movies`,
          err: JSON.stringify(e),
        };
        logger.error(errMsg);
        res.status(404)
            .json(errMsg);
      });
};

const getUserMovies = async (req, res, next) => {
  const {user_id} = req.params;
  logger.info(`Attempt to GET movies`, user_id);
  DB.user_movies_view.find({user_id})
      .then((movies) => {
        if (!movies.length > 0) {
          const errMsg = {
            err: `Movies not found`,
            code: 404,
          };
          res.status(404)
              .json(errMsg);
          return;
        } else {
          logger.debug(`Sending ${movies.length} user movies`)
          res.status(200)
              .json(movies);
        }
      })
      .catch((e) => {
        const errMsg = {
          msg: `Failed to fetch movies`,
          err: JSON.stringify(e),
        };
        logger.error(errMsg);
        res.status(404)
            .json(errMsg);
      });
};

const voteMovie = async (req, res, next) => {
  const {user_id, movie_id, vote_type} = req.body;
  logger.info(`Attempt to vote for movie`, req.body);
  if (!movie_id || !vote_type || !user_id) {
    res.status(400)
        .send({msg: 'Missing data'});
    return null;
  }
  const hasVoted = await checkIfAlreadyVoted(user_id, movie_id);
  if (hasVoted && hasVoted.vote_type === vote_type) {
    res.status(400)
        .send({msg: 'Already voted for movie'});
    return null;
  } else if (hasVoted && hasVoted.vote_type !== vote_type) {
    await DB.user_votes.destroy({user_id, movie_id});
  }
  pusher.trigger('movie-channel', 'vote', {
    "message": `We have one more ${vote_type}`
  });

  DB.user_votes.insert({
    user_id, movie_id, vote_type,
  })
      .then((vote) => {
        res.status(200).send(vote);
      })
      .catch((err) => res.status(404).json(err));
};

const unVoteMovie = async (req, res, next) => {
  const {user_id, movie_id} = req.body;
  if (!movie_id || !user_id) {
    res.status(400)
        .send({msg: 'Missing data'});
    return null;
  }
  const hasVoted = await checkIfAlreadyVoted(user_id, movie_id);
  if (!hasVoted) {
    res.status(400)
        .send({msg: 'Hasn\'t voted for movie'});
    return null;
  }
  DB.user_votes.destroy({
    user_id, movie_id
  })
      .then((vote) => {
        res.status(200).send(vote);
      })
      .catch((err) => res.status(404).json(err));
};


const checkIfAlreadyVoted = async (user_id, movie_id) => {
  const hasVoted = await DB.user_votes.find({user_id, movie_id});
  return hasVoted.length > 0;
};

module.exports = {createMovie, getMovies, getUserMovies, voteMovie, unVoteMovie}
