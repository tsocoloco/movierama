const express = require('express');
const router = express.Router();

router.use('/users', require('./userRoutes'));
router.use('/movies', require('./movieRoutes'));

module.exports = router;
