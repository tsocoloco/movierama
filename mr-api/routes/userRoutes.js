const express = require('express')
const {checkAccountToken} = require('../utils/jwtUtil');
const userRouter = express.Router();
const {userLogin, createUser, checkUser, getUserData, getUserVotes} = require('../controllers/UserController')


userRouter.post('/', createUser);
userRouter.post('/login', userLogin);
userRouter.get('/check', checkUser);
userRouter.get('/checkToken', checkAccountToken, getUserData);
userRouter.get('/:userId/votes', checkAccountToken, getUserVotes);
module.exports = userRouter;
