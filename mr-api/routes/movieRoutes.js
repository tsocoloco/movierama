const express = require('express')
const {checkAccountToken} = require('../utils/jwtUtil');
const userRouter = express.Router();
const {createMovie, getMovies, getUserMovies, voteMovie, unVoteMovie} = require('../controllers/MovieController')


userRouter.post('/', checkAccountToken, createMovie);
userRouter.post('/vote', checkAccountToken, voteMovie);
userRouter.post('/unvote', checkAccountToken, unVoteMovie);
userRouter.get('/', getMovies);
userRouter.get('/user/:user_id', getUserMovies);

module.exports = userRouter;
