/* global logger*/
const massive = require('massive');

const connectionString = process.env.DATABASE_URL;

const init = () => new Promise((resolve, reject) => {
  massive({connectionString, ssl: 'require'})
      .then(resolve)
      .catch(reject);
});

const clear = db => new Promise((res, rej) => {
  if (process.env.NODE_ENV === 'test') {
    db.clearDB().then(res).catch(rej);
  } else {
    rej(Error('Operation not supported for this environment'));
  }
});

module.exports = {
  init,
  clear,
};
