const jwt = require('jsonwebtoken');


const checkAccountToken = async (req, res, next) => {
  try {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to
    // lowercase
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }

    if (token) {
      jwt.verify(token, process.env.APP_SECRET, (err, decoded) => {
        if (err) {
          logger.trace(err.message);

          return res.json({
            success: false,
            message: 'Token is not valid'
          });
        } else {
          req.decoded = decoded;
          logger.trace('decoded',decoded);
          next();
        }
      });
    } else {
      return res.json({
        success: false,
        message: 'Auth token is not supplied'
      });
    }
  } catch (e) {
    logger.debug('Auth token missing');
    res.status(400).send('Auth token missing')
  }
};


module.exports = { checkAccountToken};
