/* global logger */

const express = require('express')
const app = express();
const router = express.Router()
require('dotenv').config();
const port = process.env.PORT || 3001;
const routes = require('./routes')
const dbService = require('./utils/dbService');
const log4js = require('log4js');
const bodyParser = require('body-parser');

global.logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL || 'trace';

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

app.use(function (err, req, res, next) {
  if (!isProduction) {
    res.status(500).send('Something broke!');
    res.json({
      errors: {
        message: err.message,
        error: err,
      }
    });
  } else {
    res.status(500).send('Something broke!')
  }
});
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
  next();
});
if (!isProduction)
  app.use(function (req, res, next) {
    logger.info('Request Type:', req.method, req.path)
    next()
  });
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/v1', routes);
app.get('/v1/health', (req, res) => res.sendStatus(200))


dbService.init().then((db) => {
  logger.debug('DB initiated');
  global.DB = db;
}).catch((e) => {
  logger.error('Failed to connect to DB', e);
});


app.listen(port, () => logger.info(`Movierama API listening on port ${port}!`))
